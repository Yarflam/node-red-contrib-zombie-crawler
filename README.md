# Node Red Contrib Zombie Crawler

Module Node-RED to use [Zombie.js](http://zombie.js.org).

Note: the module is old and no longer maintained for several years. For this reason, I decided to code a more recent version based on Puppeteer, [node-red-contrib-puppeteer-crawler](https://gitlab.com/Yarflam/node-red-contrib-puppeteer-crawler).

Although you might prefer this version, as it takes less disk space with the node modules (~35 MB vs ~420 MB).

## Features

Version 1.0.0

-   Node options
    -   **User-Agent**: define your browser
    -   **Nb Threads**: maximum number of queries performed in parallel
    -   **Wait duration**: limit of Xs for the page to respond
    -   **Verbose**: see the traces in the debug tab
    -   **Load Scripts**: load the scripts in the page
    -   **Load CSS**: load the styles in the page
-   Payload
    -   **url**: the url of the page to consult
    -   **scripts**: an array of scripts to run
    -   **scenario**: an asynchronous function to execute once the page is loaded (overwrites the scripts option **\*1**)
        -   **args**: browser:Object, end:Function, { msg:Object, scripts:Function }
            -   **browser**: the Zombie instance
            -   **end**: the function to return the data
            -   **msg**: the message received by the node
            -   **scripts**: execute the scripts **\*1**

## Install

The node module must be installed from the command line. You can use the following command from within your user data directory (by default, \$HOME/.node-red):

```bash
npm install /path/to/node-red-contrib-zombie-crawler
```

Maybe I'll put it on NPM ... if you're wise! :p

## Dev / Tests

You can also test the example I set up (`testflow.json`):

```bash
npm start
```

## Authors

-   Yarflam - _initial work_

## License

MIT License - Copyright (c) 2022 Yarflam
