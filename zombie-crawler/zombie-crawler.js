module.exports = function(RED) {
    function ZombieCrawlerNode(config) {
        var node = this;
        RED.nodes.createNode(this, config);
        /* Import */
        const Crawler = require('./core/Crawler');
        const tools = require('./core/tools');
        /* Crawler instance */
        const crawler = new Crawler();
        crawler
            .loadConfig(config)
            .pipe((msg, data = {}, errors = false) => {
                let out = [null, null];
                out[errors ? 1 : 0] = {
                    topic: msg.topic,
                    payload: Object.assign(
                        {
                            errors,
                            data
                        },
                        msg.payload
                    )
                };
                node.send(out);
            })
            .logs(msg =>
                tools.nodeRedDebug(msg, { topic: 'zombie-crawler', node, RED })
            );
        /* Interface */
        node.on('input', function(msg) {
            crawler.request(msg);
        });
    }
    RED.nodes.registerType('zombie-crawler', ZombieCrawlerNode);
};
