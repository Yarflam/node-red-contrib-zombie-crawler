const ThreadManager = require('./ThreadManager');
const tools = require('./tools');
const Zombie = require('zombie');

class Crawler {
    constructor() {
        this._config = {
            /* General options */
            userAgent: 'ZombieCrawler',
            nbThreads: 1,
            waitDuration: 30,
            notVerifyTLS: false,
            ignoreDroppedRequests: true,
            verbose: false,
            /* Browser options */
            runScripts: true,
            loadCSS: true
        };
        this._threads = new ThreadManager(this._config.nbThreads);
        this._fctPipe = null;
        this._fctLogs = null;
    }

    request(msg) {
        /* Check the parameters */
        if (!tools.pNestedSec(msg, 'payload.url', 'string')) {
            this.__logs('[ZC] Error: wrong parameters');
            return;
        }
        /* Create a new thread */
        this._threads.create(thread => this.__runBrowser(thread, msg));
        return this;
    }

    __runBrowser(thread, msg) {
        this.__logs(`[ZC] RUN #${thread.id}`);
        /* Create the browser */
        const browser = new Zombie({
            userAgent: this._config.userAgent,
            waitDuration: `${this._config.waitDuration}s`,
            runScripts: true,
            silent: true,
            features: `${this._config.runScripts ? 'scripts' : 'no-scripts'} ${
                this._config.loadCSS ? 'css' : 'no-css'
            } img iframe`
            // ignoreDroppedRequests: this._config.ignoreDroppedRequests
        });
        const close = () => {
            this.__logs(`[ZC] CLOSE #${thread.id}`);
            browser.window.close();
            browser.destroy();
            thread();
        };
        /* Request */
        const request = browser.visit(msg.payload.url);
        this.__logs(`[ZC] URL ${msg.payload.url}`);
        /* Treatment */
        request
            .then({}, e => this.__pipe(msg, null, e))
            .then(async () => {
                const scripts = async () => {
                    const payload = `(function ${this.__evalBrowser.toString()})(window, document, [${tools
                        .pNestedSec(msg, 'payload.scripts', 'array')
                        .filter(fct => tools.isFunction(fct))
                        .map(fct => fct.toString())
                        .join(',')}])`;
                    return await Promise.all(browser.evaluate(payload));
                };
                /* Mode 1. Execute Scenario */
                const scenario = tools.pNested(msg, 'payload.scenario');
                if (tools.isFunction(scenario)) {
                    scenario(
                        browser,
                        out => {
                            if (tools.isArray(out)) {
                                out.forEach(res => this.__pipe(msg, res));
                            } else this.__pipe(msg, out);
                            close();
                        },
                        { msg, scripts }
                    );
                    return;
                }
                /* Mode 2. Execute Scripts */
                (await scripts()).forEach(res => this.__pipe(msg, res));
                close();
            })
            .catch(e => {
                this.__logs(`[ZC] Error: ${e}`);
                this.__pipe(msg, null, e);
            });
    }

    __evalBrowser(window, document, scripts) {
        /* No script -> return page content */
        if (!scripts.length) return [document.querySelector('html').innerHTML];
        /* Execute the scripts */
        return scripts.map(script => script());
    }

    __pipe(msg, data, errors) {
        if (!this._fctPipe) return;
        this._fctPipe(msg, data, errors);
    }

    __logs(msg) {
        if (!this._config.verbose || !this._fctLogs) return;
        if (!tools.isString(msg) || !msg.length) return;
        this._fctLogs(msg);
    }

    loadConfig(config) {
        for (let key of Object.keys(this._config)) {
            if (!config[key]) continue;
            this._config[key] = tools.sameType(config[key], this._config[key]);
        }
        /* Update the nb threads */
        this._threads.setLimit(this._config.nbThreads);
        return this;
    }

    pipe(fct) {
        if (!tools.isFunction(fct)) return;
        this._fctPipe = fct;
        return this;
    }

    logs(fct) {
        if (!tools.isFunction(fct)) return;
        this._fctLogs = fct;
        return this;
    }
}

module.exports = Crawler;
