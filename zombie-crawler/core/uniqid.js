let uid = 0;

const uniqid = () => {
    uid = Math.max(uid + 1, new Date().getTime());
    return uid.toString(16);
};

uniqid();
module.exports = uniqid;
