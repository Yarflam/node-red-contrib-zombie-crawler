const uniqid = require('./uniqid');
const tools = require('./tools');

class ThreadManager {
    constructor(limit = 0) {
        this._limit = 1;
        this._running = [];
        this._waiting = [];
        /* Parameters */
        this.setLimit(limit);
    }

    create(worker) {
        if (!tools.isFunction(worker)) return false;
        /* Create the thread */
        let thread = () => {
            let finder;
            /* Running Thread */
            finder = this._running.indexOf(thread);
            if (finder >= 0) {
                this._running.splice(finder, 1);
                this.__next(); // finished
                return;
            }
            /* Waiting Thread */
            finder = this._waiting.indexOf(thread);
            if (finder >= 0) this._waiting.splice(finder, 1);
        };
        thread.worker = worker;
        thread.id = uniqid();
        this._waiting.push(thread);
        /* Execute the thread ... maybe */
        this.__next();
        return true;
    }

    __next() {
        let nbTodo = this._limit
            ? Math.max(0, this._limit - this._running.length)
            : this._waiting.length;
        /* Run the new threads */
        if (nbTodo) {
            this._waiting.splice(0, nbTodo).forEach(thread => {
                this._running.push(thread);
                thread.worker(thread);
            });
        }
    }

    setLimit(limit) {
        if (!tools.isNumber(limit)) return;
        this._limit = Math.max(0, Math.floor(limit));
        this.__next();
    }
}

module.exports = ThreadManager;
